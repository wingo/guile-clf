;;; clf.scm
;;;
;;; Copyright (C) 2011 Andy Wingo <wingo@pobox.com
;;; Copyright (C) 2011 Free Software Foundation, Inc.
;;;
;;; This library is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;; 
;;; This library is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; 
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;;; 02110-1301 USA
;;;

(define-module (clf)
  #:use-module (web http)
  #:use-module (web uri)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:export (line-ip
            line-user
            line-utc
            line-method
            line-uri
            line-version
            line-code
            line-bytes
            line-referer
            line-ua
            line-referer-uri

            call-with-clf
            for-each-clf-line
            clf-fold

            make-analysis
            analysis-proc
            analysis-seed

            counter-analysis
            path-analysis
            analyze-logs!))

(define (fail . reasons)
  (error "parse failed" reasons))

(define (index str char start end)
  (or (string-index str char start end)
      (fail "failed to find" str char start end)))

(define* (parse-non-negative-integer val #:optional (start 0)
                                     (end (string-length val)))
  (define (char->decimal c)
    (let ((i (- (char->integer c) (char->integer #\0))))
      (if (and (<= 0 i) (< i 10))
          i
          (fail 'non-negative-integer val))))
  (if (not (< start end))
      (fail 'non-negative-integer val)
      (let lp ((i start) (out 0))
        (if (< i end)
            (lp (1+ i)
                (+ (* out 10) (char->decimal (string-ref val i))))
            out))))

(define-syntax string-match?
  (lambda (x)
    (syntax-case x ()
      ((_ str pat) (string? (syntax->datum #'pat))
       (let ((p (syntax->datum #'pat)))
         #`(let ((s str))
             (and
              (= (string-length s) #,(string-length p))
              #,@(let lp ((i 0) (tests '()))
                   (if (< i (string-length p))
                       (let ((c (string-ref p i)))
                         (lp (1+ i)
                             (case c
                               ((#\.)   ; Whatever.
                                tests)
                               ((#\d)   ; Digit.
                                (cons #`(char-numeric? (string-ref s #,i))
                                      tests))
                               ((#\a)   ; Alphabetic.
                                (cons #`(char-alphabetic? (string-ref s #,i))
                                      tests))
                               (else    ; Literal.
                                (cons #`(eqv? (string-ref s #,i) #,c)
                                      tests)))))
                       tests)))))))))

;; "Jan" | "Feb" | "Mar" | "Apr" | "May" | "Jun"
;; "Jul" | "Aug" | "Sep" | "Oct" | "Nov" | "Dec"

(define (parse-month str start end)
  (define (bad)
    (fail 'month (substring str start end)))
  (if (not (= (- end start) 3))
      (bad)
      (let ((a (string-ref str (+ start 0)))
            (b (string-ref str (+ start 1)))
            (c (string-ref str (+ start 2))))
        (case a
          ((#\J)
           (case b
             ((#\a) (case c ((#\n) 1) (else (bad))))
             ((#\u) (case c ((#\n) 6) ((#\l) 7) (else (bad))))
             (else (bad))))
          ((#\F)
           (case b
             ((#\e) (case c ((#\b) 2) (else (bad))))
             (else (bad))))
          ((#\M)
           (case b
             ((#\a) (case c ((#\r) 3) ((#\y) 5) (else (bad))))
             (else (bad))))
          ((#\A)
           (case b
             ((#\p) (case c ((#\r) 4) (else (bad))))
             ((#\u) (case c ((#\g) 8) (else (bad))))
             (else (bad))))
          ((#\S)
           (case b
             ((#\e) (case c ((#\p) 9) (else (bad))))
             (else (bad))))
          ((#\O)
           (case b
             ((#\c) (case c ((#\t) 10) (else (bad))))
             (else (bad))))
          ((#\N)
           (case b
             ((#\o) (case c ((#\v) 11) (else (bad))))
             (else (bad))))
          ((#\D)
           (case b
             ((#\e) (case c ((#\c) 12) (else (bad))))
             (else (bad))))
          (else (bad))))))

;; CLF dates: 06/Dec/2010:06:25:34 +0000
;;            012345678901234567890123456
;;            0         1         2
(define (parse-clf-date str)
  ;; We could verify the day of the week but we don't.
  (if (not (string-match? str "dd/aaa/dddd:dd:dd:dd +dddd"))
      (fail "bad date" str))
  (let ((date (parse-non-negative-integer str 0 2))
        (month (parse-month str 3 6))
        (year (parse-non-negative-integer str 7 11))
        (hour (parse-non-negative-integer str 12 14))
        (minute (parse-non-negative-integer str 15 17))
        (second (parse-non-negative-integer str 18 20))
        (offset (+ (* (parse-non-negative-integer str 22 24)
                      3600)
                   (* (parse-non-negative-integer str 24 26)
                      60))))
    (make-date 0 second minute hour date month year offset)))

(define-syntax unless
  (syntax-rules ()
    ((_ test e e* ...)
     (if (not test) (begin e e* ...)))))

(define-record-type <line>
  (make-line ip user utc method uri version code bytes referer ua)
  line?
  (ip line-ip)
  (user line-user)
  (utc line-utc)
  (method line-method)
  (uri line-uri)
  (version line-version)
  (code line-code)
  (bytes line-bytes)
  (referer line-referer)
  (ua line-ua)

  (referer-uri %line-referer-uri set-line-referer-uri!))

(define (line-referer-uri line)
  (and (line-referer line)
       (or (%line-referer-uri line)
           (let ((uri (or (string->uri (line-referer line))
                          (fail "bad referer" line))))
             (set-line-referer-uri! line uri)
             uri))))

(define* (call-with-clf str proc #:optional
                        (start 0) (end (string-length str)))
  (let ((ip-end (index str #\space start end)))
    (unless (and (eqv? (string-ref str (+ ip-end 1)) #\-)
                 (eqv? (string-ref str (+ ip-end 2)) #\space))
      (fail "unexpected ident host"))
    (let* ((user-start (+ ip-end 3))
           (user-end (index str #\space user-start end))
           (user (if (and (= (- user-end user-start) 1)
                          (eqv? (string-ref str user-start) #\-))
                     #f
                     (substring str user-start user-end)))
           (date-start (+ user-end 2)))
      (unless (eqv? (string-ref str (1- date-start)) #\[)
        (fail "expected a brace"))
      (let ((date-end (index str #\] date-start end)))
        (unless (and (eqv? (string-ref str (1+ date-end)) #\space)
                     (eqv? (string-ref str (+ date-end 2)) #\"))
          (fail "quoi"))
        (let* ((method-start (+ date-end 3))
               (method-end (index str #\space method-start end))
               (path-start (1+ method-end))
               (path-end (index str #\space path-start end))
               ;; HACK
               (version-start (index str #\H path-end end))
               (version-end (index str #\" version-start end)))
          (unless (eqv? #\space (string-ref str (1+ version-end)))
            (fail "spacy"))
          (let* ((code-start (+ version-end 2))
                 (code-end (index str #\space code-start end))
                 (bytes-start (+ code-end 1))
                 (bytes-end (index str #\space bytes-start end)))
            (unless (eqv? #\" (string-ref str (1+ bytes-end)))
              (fail "reffy-1"))
            (let* ((referer-start (+ bytes-end 2))
                   (referer-end (index str #\" referer-start end)))
              (unless (and (eqv? #\space (string-ref str (1+ referer-end)))
                           (eqv? #\" (string-ref str (+ referer-end 2))))
                (fail "reffy-2"))
              (let* ((ua-start (+ referer-end 3))
                     (ua-end (index str #\" ua-start end)))
                (values
                 (proc
                  (make-line
                   (substring str start ip-end)
                   user
                   (time-second
                    (date->time-utc
                     (parse-clf-date (substring str date-start date-end))))
                   (parse-http-method str method-start method-end)
                   (or (parse-request-uri str path-start path-end)
                       (fail "bad path"))
                   (parse-http-version str version-start version-end)
                   (parse-non-negative-integer str code-start code-end)
                   (parse-non-negative-integer str bytes-start bytes-end)
                   (if (and (= referer-end (1+ referer-start))
                            (eqv? (string-ref str referer-start) #\-))
                       #f
                       (substring str referer-start referer-end))
                   (substring str ua-start ua-end)))
                 (if (= (1+ ua-end) end)
                      #f
                      (+ ua-end 2)))))))))))

(define (for-each-clf-line proc in)
  (catch #t
    (lambda ()
      (let lp ((line (read-line in)))
        (unless (eof-object? line)
          (begin
            (call-with-clf line proc)
            (lp (read-line in))))))
    (lambda (k . args)
      (print-exception (current-error-port) #f k args)
      (unless (eq? k 'signal)
        (for-each-clf-line proc in)))))

(define (clf-fold port proc seed)
  (for-each-clf-line
   (lambda (line)
     (set! seed (proc line seed)))
   port)
  seed)

(define-record-type <analysis>
  (make-analysis proc seed)
  analysis?
  (proc analysis-proc)
  (seed analysis-seed set-analysis-seed!))

(define (counter-analysis)
  (make-analysis (lambda (line count) (1+ count)) 0))

(define (path-analysis)
  (make-analysis (lambda (line table)
                   (let ((path (uri-path (line-uri line))))
                     (hash-set! table path
                                (1+ (hash-ref table path 0))))
                   table)
                 (make-hash-table)))

(define (analyze-logs! port analyses)
  (unless (and-map analysis? analyses)
    (error "expected a list of analyses" analyses))
  (for-each-clf-line
   (lambda (line)
     (for-each
      (lambda (a)
        (set-analysis-seed! a ((analysis-proc a) line (analysis-seed a))))
      analyses))
   port)
  analyses)
